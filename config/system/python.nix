{ pkgs, config, lib, host, ... }:

let
  inherit (import ../../hosts/${host}/options.nix) python;
  my-python-packages = ps: with ps; [
    fastapi
    gunicorn
    jupyter
    matplotlib
    nltk
    numpy
    pandas
    pydantic
    requests
    scikit-learn
    seaborn
    uvicorn
  ];
in lib.mkIf (python == true) {
  environment.systemPackages = with pkgs; [
    jetbrains.pycharm-community-bin
    (pkgs.python3.withPackages my-python-packages)
  ];

}
