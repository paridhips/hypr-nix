{ pkgs, config, username, host, ... }:

let 
  inherit (import ../../hosts/${host}/options.nix) 
    browser wallpaperDir wallpaperGit flakeDir;
in {
  # Install Packages For The User
  home.packages = with pkgs; [
#Internet
    pkgs."${browser}"
    brave
    google-chrome
    transmission-gtk
    librewolf

#Communication
    discord 
    gtkcord4
    telegram-desktop

#Hyprland

    gnome.file-roller
    godot_4 
    grim 
    libvirt
    openra font-awesome 
    rofi-wayland 
    element-desktop 
    openrgb-with-all-plugins
    rustup 
    slurp
    swayidle 
    swaylock
    swaynotificationcenter
    swww 
    xonotic 
    zeroad
 
#Development
    #sts.nix
    insomnia
    rustup
    android-studio
    geckodriver
    flutter
    jdk
    nodePackages_latest.expo-cli
    neovide 
    nodejs
    staruml
    vim 
    vscodium
#utilities
    alacritty
    android-tools
    arp-scan
    bitwarden
    cinnamon.nemo
    cliphist
    curl
    gnome.nautilus
    gparted
    libvirt 
    networkmanagerapplet
    piper
    tree
    unrar
    yt-dlp

#Audio 
    easyeffects
    qpwgraph

#Productivity
    libreoffice-fresh
    librecad

#Homelab
    nextcloud-client

#Media Manipulation
    audacity
    blender 
    gimp 
    kdenlive 
    obs-studio 
#Media
    imv 
    mpv
    supersonic-wayland
    pavucontrol 
#Fonts
    corefonts
    font-awesome
    vistafonts
#Gaming
    protontricks
    protonup-qt
    lutris
    heroic

#Extras
    gnome.file-roller

    (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
    # Import Scripts
    (import ./../scripts/emopicker9000.nix { inherit pkgs; })
    (import ./../scripts/task-waybar.nix { inherit pkgs; })
    (import ./../scripts/squirtle.nix { inherit pkgs; })
    (import ./../scripts/wallsetter.nix { inherit pkgs; inherit wallpaperDir;
      inherit username; inherit wallpaperGit; })
    (import ./../scripts/themechange.nix { inherit pkgs; inherit flakeDir; inherit host; })
    (import ./../scripts/theme-selector.nix { inherit pkgs; })
    (import ./../scripts/nvidia-offload.nix { inherit pkgs; })
    (import ./../scripts/web-search.nix { inherit pkgs; })
    (import ./../scripts/rofi-launcher.nix { inherit pkgs; })
    (import ./../scripts/screenshootin.nix { inherit pkgs; })
    (import ./../scripts/list-hypr-bindings.nix { inherit pkgs; inherit host; })
  ];

  programs.gh.enable = true;
}
